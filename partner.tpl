<main>
    <section class="category-breadcrumb bg-blackblue bg-lines big-capture mb120">
        <div class="container">
            <div class="row big-capture__wrap">
                <div class="col-sm-6">
                    <div class="big-capture__content">
                        <div class="box-heading">
                            <h1 class="page-title category-title">Партнёрская <br> программа F+ tech  </h1>
                        </div>
                        <div class="big-capture__subtitle">
                            <p>Создаем лучшие решения вместе</p>
                        </div>
                        <div class="big-capture__btn">
                            <a href="#maintarget" class="ya-link btn-white">Стать партнером</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="big-capture__img">
                        <img src="/image/partner/img-1.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="advantage mb120">
        <div class="container">
            <div class="advantage__wrap slick-slider">
                <div class="advantage__item">
                    <div class="advantage__icon advantage__icon-1">
                        <span></span>
                    </div>
                    <div class="advantage__text fz16">
                        <span>Рекламно-маркетинговая поддержка</span>
                    </div>
                </div>
                <div class="advantage__item">
                    <div class="advantage__icon advantage__icon-2">
                        <span></span>
                    </div>
                    <div class="advantage__text fz16">
                        <span>Поддержка партнера
                            независимо от статуса</span>
                    </div>
                </div>
                <div class="advantage__item">
                    <div class="advantage__icon advantage__icon-3">
                        <span></span>
                    </div>
                    <div class="advantage__text fz16">
                        <span>Открытость для партнера, доступность ресурсов</span>
                    </div>
                </div>
                <div class="advantage__item">
                    <div class="advantage__icon advantage__icon-4">
                        <span></span>
                    </div>
                    <div class="advantage__text fz16">
                        <span>Помощь в организации маркетинговых мероприятий</span>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    <section class="theses mb120">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="box">
                        <div class="box-heading">
                            <p>Программа в 3 тезисах:</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row theses__wrap">
                <div class="col-sm-6">
                    <div class="big-capture__img">
                        <img src="/image/partner/img-2.jpg" alt="">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="theses__box slick-slider">
                        <div class="theses__item">
                            <span class="h3">01 /</span>
                            <span class="fz16">Продукты и решения F+ tech продаются 
                                только через сеть авторизованных партнеров</span>
                        </div>
                        <div class="theses__item">
                            <span class="h3">02 /</span>
                            <span class="fz16">Наряду с заказчиками, партнеры – обязательная
                                и неотъемлемая часть бизнеса F+ tech</span>
                        </div>
                        <div class="theses__item">
                            <span class="h3">03 /</span>
                            <span class="fz16">Защита интересов добросовестного партнера – обязанность F+ tech</span>
                        </div>
                    </div>
                    <div class="theses__footer">
                        <p class="fz16">Полное описание Партнерской программы F+ tech<a class="fz16 color-blackblue" href="/pdf/rules.pdf" download target="_blank">здесь</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="types-partners mb120">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="box">
                        <div class="box-heading">
                            <p>Типы партнеров</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row types-partners__wrap">
                <div class="col-sm-4">
                    <div class="types-partners__item bg-item">
                        <p class="h3 mb20">Реселлер</p>
                        <span class="fz16">
                            Партнер, имеющий право приобретать продукцию у дистрибьютора
и перепродавать ее напрямую клиенту
                        </span>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="types-partners__item bg-item">
                        <p class="h3 mb20">Системный интегратор</p>
                        <span class="fz16">
                            Партнер, имеющий право на поставку приобретенной у дистрибьютора продукции вендора под брендом вендора в составе комплексных решений напрямую клиенту, а также
на выполнение работ с поставленным оборудованием
                        </span>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="types-partners__item bg-item">
                        <p class="h3 mb20">Дистрибьютор</p>
                        <span class="fz16">
                            Партнер, имеющий право поставлять
и реализовывать продукцию вендора реселлерам и системным интеграторам, исключая прямые продажи клиенту
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="min-capture bg-blackblue bg-lines mb120">
        <div class="container">
            <div class="row min-capture__wrap">
                <div class="col-md-9 col-sm-8">
                    <div class="min-capture__title">
                        <span>Хотите участвовать в Партнерской программе?</span>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4">
                    <div class="min-capture__btn">
                        <a href="#maintarget" class="ya-link btn-white fz16 fw600">Стать партнером</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="levels mb120">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="box">
                        <div class="box-heading">
                            <p>Уровни партнерства системных интеграторов</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row levels__wrap">
                <div class="col-sm-4">
                    <div class="levels__item levels__item_green">
                        <div class="levels__header">
                            <div class="levels__title bg-item">
                                <span class="h3">Авторизованный Партнер</span>
                            </div>
                            <div class="levels__content bg-item">
                                <ul>
                                    <li>Оборот Партнера по продуктам Вендора $0,3M в год</li>
                                    <li>1 обученный менеджер</li>
                                    <li>Бизнес-план на год</li>
                                </ul>
                            </div>
                        </div>
                        <div class="levels__footer bg-item">
                            <div class="levels__content">
                                <ul>
                                    <li>Регистрация сделок</li>
                                    <li>Партнерский сертификат</li>
                                    <li>Информационная поддержка</li>
                                    <li>Указание в списке партнеров на сайте</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="levels__item levels__item_blue">
                        <div class="levels__header">
                            <div class="levels__title bg-item">
                                <span class="h3">Серебряный Партнер</span>
                            </div>
                            <div class="levels__content bg-item">
                                <ul>
                                    <li>Оборот Партнера по продуктам Вендора $0,3M в год</li>
                                    <li>Логотип вендора на сайте</li>
                                    <li>2 обученных менеджер</li>
                                    <li>1 обученный инженер</li>
                                    <li>Бизнес-план на год</li>
                                </ul>
                            </div>
                        </div>
                        <div class="levels__footer bg-item">
                            <div class="levels__content">
                                <ul>
                                    <li class="levels__arrow levels__arrow_green">Регистрация сделок</li>
                                    <li class="levels__arrow levels__arrow_green">Партнерский сертификат</li>
                                    <li class="levels__arrow levels__arrow_green">Информационная поддержка</li>
                                    <li class="levels__arrow levels__arrow_green">Указание в списке партнеров на сайте</li>
                                    <li>Специальные цены на демо</li>
                                    <li>Доступ к конфигуратору</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="levels__item levels__item_red">
                        <div class="levels__header">
                            <div class="levels__title bg-item">
                                <span class="h3">Золотой Партнер</span>
                            </div>
                            <div class="levels__content bg-item">
                                <ul>
                                    <li>Оборот Партнера по продуктам Вендора $1M в год</li>
                                    <li>Логотип вендора на сайте</li>
                                    <li>2 обученных менеджер</li>
                                    <li>2 обученных инженера</li>
                                    <li>Бизнес-план на год</li>
                                </ul>
                            </div>
                        </div>
                        <div class="levels__footer bg-item">
                            <div class="levels__content">
                                <ul>
                                    <li class="levels__arrow levels__arrow_green">Регистрация сделок</li>
                                    <li class="levels__arrow levels__arrow_green">Партнерский сертификат</li>
                                    <li class="levels__arrow levels__arrow_green">Информационная поддержка</li>
                                    <li class="levels__arrow levels__arrow_green">Указание в списке партнеров на сайте</li>
                                    <li class="levels__arrow levels__arrow_blue">Специальные цены на демо</li>
                                    <li class="levels__arrow levels__arrow_blue">Доступ к конфигуратору</li>
                                    <li>Бюджет на совместный маркетинг</li>
                                    <li>Выделенный партнерский менеджер</li>
                                    <li>Разработка индивидуальной стратегии</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="rules bg-lightgrey">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="box">
                        <div class="box-heading">
                            <p>Уровни партнерства системных интеграторов</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row rules__wrap">
                <div class="col-sm-6">
                    <div class="rules__text">
                        <p class="fz16">
                            Все участники, вступающие в программу, в обязательном порядке соглашаются с положением
                            <a href="/pdf/rules.pdf" class="color-blackblue" download target="_blank">Партнерской программы</a>
                        </p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="rules__text">
                        <p class="fz16">
                            F+ tech оставляет за собой право пересматривать, ограничивать, отменять или не предоставлять партнерский статус по собственному усмотрению, а также в случае нарушения партнером условий сотрудничества, возникновения возможных юридических, финансовых, репутационных и других рисков
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="category-breadcrumb bg-blackblue bg-lines big-capture big-capture-2 big-capture_mtunset mb120">
        <div class="container">
            <div class="row big-capture__wrap">
                <div class="col-sm-6">
                    <div class="big-capture__content">
                        <div class="box-heading">
                            <h1 class="page-title category-title">Оставьте заявку <br>
                                на участие в программе  </h1>
                        </div>
                        <div class="big-capture__btn">
                            <a href="#maintarget" class="ya-link btn-white">Стать партнером</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="big-capture__img">
                        <img src="/image/partner/img-3.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="maintarget" class="howreg mb120">
        <div class="container">
            <div class="howreg__header">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="box">
                            <div class="box-heading">
                                <p>Как зарегистрироваться</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <p class="fz16">Вступайте в сообщество профессионалов, строящих высокотехнологичные IT-инфраструктуры будущего!</p>
                    </div>
                </div>
            </div>
            <div class="howreg__box">
                <div class="howreg__item">
                    <div class="howreg__number">
                        <div class="howreg__icon">
                            <span>01</span>
                        </div>
                    </div>
                    <div class="howreg__content">
                        <p class="howreg__text fz16">
                            Cкачайте и заполните
                            согласие на участие
                            в Партнерской программе
                            и бизнес-план
                        </p>
                        <div class="howreg__lins">
                            <div class="howreg__link howreg__download">
                                <a href="" target="_blank" download="" class="fz16 disabled" disabled>Скачать</a>
                            </div>
                            <div class="howreg__link howreg__document">
                                <a href="/pdf/agree-term.pdf" target="_blank" class="fz16" download>Согласие на участие
                                    в Партнерской программе</a>
                            </div>
                            <div class="howreg__link howreg__document">
                                <a href="/pdf/biz-plan.docx" target="_blank" class="fz16">Бизнес-план</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="howreg__item">
                    <div class="howreg__number">
                        <div class="howreg__icon">
                            <span>02</span>
                        </div>
                    </div>
                    <div class="howreg__content">
                        <p class="howreg__text fz16">
                            Прикрепите заполненные документы (согласие на участие и бизнес-план) к форме <a href="#partnerform" class="ya-link color-blackblue link-underline">обратной связи</a>
                        </p>
                    </div>
                </div>
                <div class="howreg__item">
                    <div class="howreg__number">
                        <div class="howreg__icon">
                            <span>03</span>
                        </div>
                    </div>
                    <div class="howreg__content">
                        <p class="howreg__text fz16">
                            Дождитесь подтверждения от партнерского менеджера F+ tech
                        </p>
                    </div>
                </div>
                <div class="howreg__item">
                    <div class="howreg__number">
                        <div class="howreg__icon">
                            <span>04</span>
                        </div>
                    </div>
                    <div class="howreg__content">
                        <p class="howreg__text fz16">
                            Получите сертификат, подтверждающий статус партнера
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="form-block bg-blackblue bg-lines form-box">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="box">
                        <div class="box-heading">
                            <p>Форма обратной связи</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-sm-12">
                    <div class="form-wrap">
                        <form id="partnerform" class="form-sobfeedback formcatapage form-partners" data-toggle="validator" role="form">
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" placeholder="Введите ваше имя*" required>
                            </div>
                            <div class="form-group">
                                <input type="text" name="email" class="form-control" placeholder="Введите ваше e-mail*" required>
                            </div>
                            <div class="form-group">
                                <input type="text" name="telephone" class="form-control" id="partnerform_telephone" placeholder="Введите ваше номер телефона">
                            </div>
                            <div class="form-group">
                                <input type="text" name="message" class="form-control" placeholder="Введите ваше сообщение">
                            </div>
                            <div class="form-group upload-file__sect">
                                <input type="file" name="partnerform-file" id="partnerform-file" required multiple class="bigform-file-field" accept=".doc,.docx,.jpg,.jpeg,.png,.zip,.7z,.iso,.tar,.tar.gz,.tzz">
                                <label class="upload-file__label" for="partnerform-file">
                                    <svg class="upload-file__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" fill="#26384a">
                                        <path d="M286 384h-80c-14.2 1-23-10.7-24-24V192h-87.7c-17.8 0-26.7-21.5-14.1-34.1L242.3 5.7c7.5-7.5 19.8-7.5 27.3 0l152.2 152.2c11.6 11.6 3.7 33.1-13.1 34.1H320v168c0 13.3-10.7 24-24 24zm216-8v112c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-23-23V366c0-13.3 10.7-24 24-24h136v8c0 31 24.3 56 56 56h80c30.9 0 55-26.1 57-55v-8h135c13.3 0 24 10.6 24 24zm-124 88c0-11-9-20-19-20s-19 9-20 20 9 19 20 20 21-9 20-20zm64 0c0-12-9-20-20-20s-20 9-19 20 9 20 20 20 21-9 20-20z">
                                        </path>
                                    </svg>
                                    <span class="upload-file__text">Загрузить файл*</span>
                                    
                                </label>
                                <ul id="lister-files"></ul>
                            </div>
                            <div class="form-group checker-line">
                                <input id="form-checker" required="" type="checkbox">
                                <label for="form">
                                    Нажимая кнопку, я подтверждаю свое ознакомление и согласие с <a href="/politika-konfidencialnosti/" target="_blank">Политикой Конфиденциальности</a> и даю <a href="/soglasie-na-obrabotku-personalnih-dannih-fizicheskim-licom-polzovatelem/" target="_blank">Согласие на обработку персональных данных</a>
                                </label>
                            </div>
                                
                            <div class="form-group">
                                <button type="submit" class="btn-white" disabled>Отправить</button>
                            </div>
                        </form>
                        <script>
                            $('#form-checker').on('change', function(){
                                if($(this).is(':checked')) {
                                    $('.btn-white').attr('disabled', false);
                                } else {
                                    $('.btn-white').attr('disabled', true);
                                }
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    
</main>
    

    <script>
        $('.slick-slider').slick({
        dots: true,
        infinite: false,
        speed: 300,
        responsive: [
            {
            breakpoint: 9999,
            settings: 'unslick'
            },
            {
            breakpoint: 767,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
            },
            {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
            }
        ]
        });
        
        $('.ya-link').on('click', function(e){
            e.preventDefault;
            var cel = $(this).attr('href');
            var tar = $(cel).offset().top - 120;
            $('body, html').animate({scrollTop: tar,}, 200);
            return false;
        });
        
        var $myForm = $('#partnerform');
        
        $myForm.validator().on('submit', function(e){
            if($('.form-control').val().trim() != ''){
                e.preventDefault();
                var form_data = new FormData(this);
                $.ajax({
                    method: 'POST',
                    url: '/index.php?route=extension/module/bigform/sendPartner',
                    processData: false,
                    contentType: false,
                    data: form_data,
                    beforeSend: function() {
                        $('#bigform_loader').css('display', 'flex');
                      },
                    success: function (data) {
                        if(data.error){
                            
                        } else {
                            $('#partnerform').before('<div class="alert alert-success">Ваша заявка успешно отправлена!</div>');    
                        }
                    },
                });
            } 
        });

        $("#partnerform_telephone").mask("+7 (999) 999-99-99");
        
        $('#partnerform-file').change(function(){
            let files = this.files;
            for (let i=0; i < files.length ; i++){
                $('#lister-files').append('<li>' + files[i].name + '</li>');    
            }
            
        });
        
    </script>

